using System;
using MonnaieMVC.Convertisseur.Controllers;
using NUnit.Framework;

namespace MonnaieMVCUnitTest
{
    public class Tests
    {
        private CurEx_Controller _controller;
        
        [SetUp]
        public void Setup()
        {
            _controller = new CurEx_Controller(null);
        }

        [Test]
        public void Assert_Wrong_From_Throw_Error()
        {
            Assert.IsFalse(_controller.SetFrom("test ahahaha"));
        }

        [Test]
        public void Assert_Convert_Work()
        {
            _controller.SetFrom("CHF");
            _controller.SetTo("EUR");
            Assert.AreEqual(9.3, _controller.Convert(10));
            Assert.AreEqual(9.3, _controller.Convert("10"));
            
            _controller.SetTo("USD");
            Assert.AreEqual(67.2,_controller.Convert(60));
            
            _controller.SetFrom("EUR");
            Assert.AreEqual(6.05,_controller.Convert(5));
            
            _controller.SetTo("CHF");
            Assert.AreEqual(10.75268817204301,_controller.Convert(10));
            
            _controller.SetFrom("USD");
            Assert.AreEqual(8.928571428571427,_controller.Convert(10));
        }
    }
}