﻿using System.Collections.Generic;

namespace MonnaieMVC.Convertisseur
{
    public abstract class CurEx_Model
    {
        public abstract List<string> AvailableCurrencies { get; }
        public abstract string From { get; set; }
        public abstract string To { get; set; }

        public abstract double Convert(double amount);
    }
}