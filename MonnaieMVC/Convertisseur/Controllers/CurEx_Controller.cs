﻿using System;
using System.Globalization;
using System.Windows.Forms;
using MonnaieMVC.Convertisseur.Models;

namespace MonnaieMVC.Convertisseur.Controllers
{
    public class CurEx_Controller
    {
        private CurEx_Model _model;
        private Form _view;

        public CurEx_Model Model => _model;

        public Form View => _view;

        public CurEx_Controller(Form view)
        {
            _view = view;
            _model = new CurEx_Model_Simple();
        }

        public double Convert(double amount)
        {
            return _model.Convert(amount);
        }
        public double Convert(string amount)
        {
            double res;
            if (double.TryParse(amount, NumberStyles.Number, CultureInfo.InvariantCulture, out res))
                return Convert(res);

            return -1;
        }

        public bool SetFrom(string currency)
        {
            bool exist = _model.AvailableCurrencies.Contains(currency);
            if (exist)    
                _model.From = currency;
            return exist;
        }
        public bool SetTo(string currency)
        {
            bool exist = _model.AvailableCurrencies.Contains(currency);
            if (exist)    
                _model.To = currency;
            return exist;
        }

        public string[] GetAvailableCurrenciesInModel()
        {
            return _model.AvailableCurrencies.ToArray();
        }
    }
}