﻿using System.Collections.Generic;

namespace MonnaieMVC.Convertisseur.Models
{
    public class CurEx_Model_Simple : CurEx_Model
    {
        private List<string> _availableCurrencies;
        private int _fromIndex;
        private int _toIndex;
        private double[,] _exchangeRate;

        public override List<string> AvailableCurrencies => _availableCurrencies;

        public override string From
        {
            get
            {
                return _availableCurrencies[_fromIndex];
            }
            set
            {
                _fromIndex = _availableCurrencies.IndexOf(value);
            }
        }

        public override string To {
            get
            {
                return _availableCurrencies[_toIndex];
            }
            set
            {
                _toIndex = _availableCurrencies.IndexOf(value);
            }}

        public CurEx_Model_Simple()
        {
            _availableCurrencies = new List<string>();
            _availableCurrencies.Add("CHF");
            _availableCurrencies.Add("EUR");
            _availableCurrencies.Add("USD");
            
            _exchangeRate = new double[_availableCurrencies.Count,_availableCurrencies.Count];
            _exchangeRate[0,0] = 1;
            _exchangeRate[1,1] = 1;
            _exchangeRate[2,2] = 1;
            _exchangeRate[0,2] = 1.12;
            _exchangeRate[2,0] = 1.0 / 1.12;
            _exchangeRate[0,1] = 0.93;
            _exchangeRate[1,0] = 1.0 / 0.93;
            _exchangeRate[1,2] = 1.21;
            _exchangeRate[2,1] = 1.0 / 1.21;
        }

        public override double Convert(double amount)
        {
            return amount * _exchangeRate[_fromIndex, _toIndex];
        }
    }
}