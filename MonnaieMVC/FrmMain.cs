﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonnaieMVC.Convertisseur.Controllers;

namespace MonnaieMVC
{
    public partial class FrmMain : Form
    {
        private readonly Regex _tbxAmountPattern = new("([0-9]|\\.)*");
        
        private CurEx_Controller _controller;
        
        public FrmMain()
        {
            InitializeComponent();

            _controller = new CurEx_Controller(this);
        }

        private void btns_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.Name)
            {
                case "btnConvert":
                    _controller.SetFrom(cbxMontantBase.Text);
                    _controller.SetTo(cbxConvert.Text);

                    double res;
                    if (double.TryParse(tbxAmount.Text, NumberStyles.Number, CultureInfo.InvariantCulture, out res))
                        lblResult.Text = _controller.Convert(res).ToString();
                    else
                        MessageBox.Show("Le chiffre a convertir est incorrect !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            cbxMontantBase.DataSource = _controller.GetAvailableCurrenciesInModel();
            cbxConvert.DataSource = _controller.GetAvailableCurrenciesInModel();
        }

        private void tbxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Back)
                e.Handled = _tbxAmountPattern.Match(e.KeyChar.ToString()).ToString() == "";
        }
    }
}