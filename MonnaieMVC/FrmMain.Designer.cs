﻿namespace MonnaieMVC
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxMontantBase = new System.Windows.Forms.ComboBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConvert = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxConvert = new System.Windows.Forms.ComboBox();
            this.tbxAmount = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbxMontantBase
            // 
            this.cbxMontantBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMontantBase.FormattingEnabled = true;
            this.cbxMontantBase.Location = new System.Drawing.Point(173, 104);
            this.cbxMontantBase.Name = "cbxMontantBase";
            this.cbxMontantBase.Size = new System.Drawing.Size(170, 24);
            this.cbxMontantBase.TabIndex = 1;
            // 
            // lblResult
            // 
            this.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblResult.Location = new System.Drawing.Point(12, 314);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(331, 127);
            this.lblResult.TabIndex = 3;
            this.lblResult.Text = "Résultat";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Monnaie de base";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Convertir en";
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(73, 234);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(213, 36);
            this.btnConvert.TabIndex = 6;
            this.btnConvert.Text = "Convertir";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btns_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Montant";
            // 
            // cbxConvert
            // 
            this.cbxConvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxConvert.FormattingEnabled = true;
            this.cbxConvert.Location = new System.Drawing.Point(173, 155);
            this.cbxConvert.Name = "cbxConvert";
            this.cbxConvert.Size = new System.Drawing.Size(170, 24);
            this.cbxConvert.TabIndex = 2;
            // 
            // tbxAmount
            // 
            this.tbxAmount.Location = new System.Drawing.Point(173, 51);
            this.tbxAmount.Name = "tbxAmount";
            this.tbxAmount.Size = new System.Drawing.Size(170, 22);
            this.tbxAmount.TabIndex = 9;
            this.tbxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAmount_KeyPress);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 450);
            this.Controls.Add(this.tbxAmount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.cbxConvert);
            this.Controls.Add(this.cbxMontantBase);
            this.Name = "FrmMain";
            this.Text = "Convertisseur";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.ComboBox cbxConvert;
        private System.Windows.Forms.TextBox tbxAmount;

        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.ComboBox cbxMontantBase;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;

        #endregion
    }
}